<?php

namespace R1KO\TimesheetScraper\Providers\Gitlab;

use R1KO\TimesheetScraper\Contracts\IProviderCredentials;
use R1KO\TimesheetScraper\Contracts\IRepositoryDataModel;
use R1KO\TimesheetScraper\Contracts\IRepositoriesDataRepository;
use R1KO\TimesheetScraper\Contracts\IRepositoriesProvider;
use R1KO\TimesheetScraper\Providers\Gitlab\GitlabRepositoryDataModel;
use R1KO\TimesheetScraper\Libs\Gitlab as GitlabClient;

class GitlabRepository implements IRepositoriesProvider
{
    /** @var IProviderCredentials */
    private $credentials;

    public function getIdent(): string
    {
        return 'gitlab';
    }

    public function run(IProviderCredentials $credentials, IRepositoriesDataRepository $repository): void
    {
        $this->credentials = $credentials;
        $client = new GitlabClient($credentials);

        $projects = $client->getProjects();

        foreach ($projects as $project) {
            $this->syncCommitsByProject($client, $project, $repository);
        }
    }

    private function syncCommitsByProject($client, array $project, IRepositoriesDataRepository $repository): void
    {
        $commits = $client->getCommitsByProject($project);

        foreach ($commits as $commit) {
            $commit['id_project'] = $project['id'];
            $this->syncCommit($commit, $repository);
        }
    }

    private function syncCommit(array $commit, IRepositoriesDataRepository $repository): void
    {
        $model = $this->createModelFromArray($commit);
        $existsModel = $repository->findByHash($this->getIdent(), $model->getHash());

        if ($existsModel) {
            return;
        }

        $repository->add($model);
    }

    private function createModelFromArray(array $data): IRepositoryDataModel
    {
        $data['source_ident'] = $this->getIdent();
        $data['id_author'] = $this->credentials->getUser()->getID();

        return new GitlabRepositoryDataModel($data);
    }
}
