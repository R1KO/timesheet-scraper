<?php

namespace R1KO\TimesheetScraper\Contracts;

interface IssuesDataRepositoryInterface
{
    public function add(ITicketsDataModel $model): void;
    public function findByRemoteID(string $source, string $remoteID): ?ITicketsDataModel;
}
