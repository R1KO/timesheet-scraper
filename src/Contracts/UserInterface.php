<?php

namespace R1KO\TimesheetScraper\Contracts;

interface UserInterface
{
    public function getID(): int;
}
