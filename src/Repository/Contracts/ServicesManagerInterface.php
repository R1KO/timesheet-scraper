<?php

namespace R1KO\TimesheetScraper\Repository\Contracts;

interface ServicesManagerInterface
{
    public function getAll(): iterable;
}
