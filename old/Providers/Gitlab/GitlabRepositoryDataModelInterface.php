<?php

namespace R1KO\TimesheetScraper\Providers\Gitlab;

use R1KO\TimesheetScraper\Contracts\IRepositoryDataModel;
use R1KO\TimesheetScraper\Providers\AbstractModel;

class GitlabRepositoryDataModelInterface extends AbstractModel implements IRepositoryDataModel
{
    public function getHash(): string
    {
        return (string) $this->get('id');
    }

    public function getMessage(): string
    {
        return (string) $this->get('message');
    }

    public function getCreated(): \DateTimeInterface
    {
        return new \DateTime($this->get('created_at'));
    }

    public function getStatsAdditions(): int
    {
        return (int) $this->get('stats')['additions'];
    }

    public function getStatsDeletions(): int
    {
        return (int) $this->get('stats')['deletions'];
    }

    public function getStatsTotal(): int
    {
        return (int) $this->get('stats')['total'];
    }

    public function getUrl(): string
    {
        return (string) $this->get('web_url');
    }
}