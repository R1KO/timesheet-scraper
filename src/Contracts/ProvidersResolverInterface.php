<?php

namespace R1KO\TimesheetScraper\Contracts;

interface ProvidersResolverInterface
{
    public function getInstance(IProviderCredentials $credentials): IScraperProvider;
}
