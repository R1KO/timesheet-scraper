<?php

namespace R1KO\TimesheetScraper\Providers\Jira;

use R1KO\TimesheetScraper\Contracts\IProviderCredentials;
use R1KO\TimesheetScraper\Contracts\ITicketsDataRepository;
use R1KO\TimesheetScraper\Contracts\ITicketsProvider;

class Jira implements ITicketsProvider
{
    public function getIdent(): string
    {
        return 'jira';
    }

    public function run(IProviderCredentials $credentials, ITicketsDataRepository $repository): void
    {
        // TODO: Implement run() method.
        // https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-issue-worklogs/#api-rest-api-3-worklog-list-post
    }


    public function getCommitsByProject(array $project): iterable
    {
        // https://docs.gitlab.com/ee/api/commits.html#list-repository-commits
        // with_stats

        return json_decode(
            <<<EOD
          [
  {
    "self": "https://your-domain.atlassian.net/rest/api/3/issue/10010/worklog/10000",
    "author": {
      "self": "https://your-domain.atlassian.net/rest/api/3/user?accountId=5b10a2844c20165700ede21g",
      "accountId": "5b10a2844c20165700ede21g",
      "displayName": "Mia Krystof",
      "active": false
    },
    "updateAuthor": {
      "self": "https://your-domain.atlassian.net/rest/api/3/user?accountId=5b10a2844c20165700ede21g",
      "accountId": "5b10a2844c20165700ede21g",
      "displayName": "Mia Krystof",
      "active": false
    },
    "comment": {
      "type": "doc",
      "version": 1,
      "content": [
        {
          "type": "paragraph",
          "content": [
            {
              "type": "text",
              "text": "I did some work here."
            }
          ]
        }
      ]
    },
    "updated": "2021-06-24T02:29:03.380+0000",
    "visibility": {
      "type": "group",
      "value": "jira-developers"
    },
    "started": "2021-06-24T02:29:03.379+0000",
    "timeSpent": "3h 20m",
    "timeSpentSeconds": 12000,
    "id": "100028",
    "issueId": "10002"
  }
]
EOD
        );
    }

}
