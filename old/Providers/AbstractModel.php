<?php

namespace R1KO\TimesheetScraper\Providers;

abstract class AbstractModel
{
    private $values;

    public function __construct(array $data)
    {
        $this->values = $data;
    }

    protected function get(string $key)
    {
        if (!array_key_exists($key, $this->values)) {
            throw new \Exception(sprintf('Unknown param "%s"', $key));
        }

        return $this->values[$key];
    }

    public function getSourceIdent(): string
    {
        return (int) $this->get('source_ident');
    }

    //    public function getID(): int
    //    {
    //        return (int) $this->get('id');
    //    }

    public function getAuthorID(): int
    {
        return (int) $this->get('id_author');
    }

    public function getProjectID(): string
    {
        return (string) $this->get('id_project');
    }

}
