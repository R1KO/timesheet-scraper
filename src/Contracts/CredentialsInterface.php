<?php

namespace R1KO\TimesheetScraper\Contracts;

interface CredentialsInterface
{
    public function getUserID(): int;
    public function getToken(): string;
}
