<?php

namespace R1KO\TimesheetScraper\Contracts;

interface ScraperProviderInterface
{
    public function getIdent(): string;
}
