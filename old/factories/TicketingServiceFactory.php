<?php

namespace plugin\Services\factories;

use plugin\Services\domain\ITicketingService;

class TicketingServiceFactory
{
    public function createByIdent(string $type): ITicketingService
    {
        $className = '\plugin\Services\domain\ticketing\\'.ucfirst($type);

        return new $className();
    }
}
