<?php

namespace R1KO\TimesheetScraper\Repository\Contracts;

use Countable;
use Iterator;

interface DataModelCollectionInterface extends Iterator, Countable
{
    /**
     * @return DataModelInterface[] $model
     */
    public function getAll(): iterable;
}
