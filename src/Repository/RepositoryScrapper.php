<?php

namespace R1KO\TimesheetScraper;

use DateTimeInterface;
use Exception;
use Psr\Container\ContainerInterface;
use R1KO\TimesheetScraper\Repository\Contracts\DataRepositoryInterface;
use R1KO\TimesheetScraper\Repository\Contracts\ServicesManagerInterface;
use R1KO\TimesheetScraper\Contracts\ServiceInterface;
use R1KO\TimesheetScraper\Repository\Contracts\ProviderInterface;

class RepositoryScrapper
{
    private $_container;

    public function __construct(ContainerInterface $container)
    {
        $this->_container = $container;
    }

    public function run(
        ServicesManagerInterface $servicesManager,
        DataRepositoryInterface $repository,
        DateTimeInterface $dateFrom,
        DateTimeInterface $dateTo
    ): void
    {
        foreach ($servicesManager->getAll() as $service) {
            $provider = $this->getProviderInstance($service);

            $provider->run(
                $service->getCredentials(),
                $repository,
                $dateFrom,
                $dateTo
            );
        }
    }

    private function getProviderInstance(ServiceInterface $service): ProviderInterface
    {
        $className = $service->getClassname();
        if (!class_exists($className)) {
            throw new Exception(sprintf('Provider class "%s" not found!', $className));
        }

        $instance = $this->_container->get($className);
        if (!$instance instanceof ProviderInterface) {
            throw new Exception(
                sprintf('Provider class "%s" is not implements "%s"!', $className, ProviderInterface::class)
            );
        }

        return $instance;
    }
}
