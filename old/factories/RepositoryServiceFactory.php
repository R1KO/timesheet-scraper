<?php

namespace plugin\Services\factories;

use plugin\Services\domain\IRepositoryService;

class RepositoryServiceFactory
{
    public function createByIdent(string $type): IRepositoryService
    {
        $className = '\plugin\Services\domain\repository\\'.ucfirst($type);

        return new $className();
    }
}
