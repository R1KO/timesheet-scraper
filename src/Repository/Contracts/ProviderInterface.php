<?php

namespace R1KO\TimesheetScraper\Repository\Contracts;

use DateTimeInterface;
use R1KO\TimesheetScraper\Contracts\CredentialsInterface;

interface ProviderInterface
{
    public function run(
        CredentialsInterface $credentials,
        DataRepositoryInterface $repository,
        DateTimeInterface $dateFrom,
        DateTimeInterface $dateTo
    ): void;
}
