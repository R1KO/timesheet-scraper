<?php

namespace R1KO\TimesheetScraper\Contracts;

interface ServiceInterface
{
    public function getCredentials(): CredentialsInterface;
    public function getClassname(): string;
}
