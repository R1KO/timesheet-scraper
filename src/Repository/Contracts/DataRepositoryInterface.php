<?php

namespace R1KO\TimesheetScraper\Repository\Contracts;

interface DataRepositoryInterface
{
    public function add(DataModelInterface $model): void;
    public function addBatch(DataModelCollectionInterface $models): void;
    public function findByHash(string $source, string $hash): ?DataModelInterface;
}
