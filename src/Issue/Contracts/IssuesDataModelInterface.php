<?php

namespace R1KO\TimesheetScraper\Contracts;

interface IssuesDataModelInterface
{
    //    public function getID(): int;
    public function getSourceIdent(): string;
    public function getAuthorID(): int;
    public function getProjectID(): string;
    public function getRemoteID(): string;
    public function getMessage(): string;
    public function getCreated(): \DateTimeInterface;
    public function getStatsAdditions(): int;
    public function getStatsDeletions(): int;
    public function getStatsTotal(): int;
    public function getUrl(): string;
}
