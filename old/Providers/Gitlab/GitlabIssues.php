<?php

namespace R1KO\TimesheetScraper\Providers\Gitlab;

use R1KO\TimesheetScraper\Contracts\IProviderCredentials;
use R1KO\TimesheetScraper\Contracts\ITicketsDataModel;
use R1KO\TimesheetScraper\Contracts\ITicketsDataRepository;
use R1KO\TimesheetScraper\Contracts\ITicketsProvider;
use R1KO\TimesheetScraper\Providers\Gitlab\GitlabIssueDataModel;
use R1KO\TimesheetScraper\Libs\Gitlab as GitlabClient;

class GitlabIssues implements ITicketsProvider
{
    /** @var IProviderCredentials */
    private $credentials;

    public function getIdent(): string
    {
        return 'gitlab';
    }

    public function run(IProviderCredentials $credentials, ITicketsDataRepository $repository): void
    {
        $this->credentials = $credentials;
        $client = new GitlabClient($credentials);

        $projects = $client->getProjects();

        foreach ($projects as $project) {
            $this->syncCommitsByProject($client, $project, $repository);
        }
    }

    private function syncCommitsByProject($client, array $project, ITicketsDataRepository $repository): void
    {
        $commits = $client->getCommitsByProject($project);

        foreach ($commits as $commit) {
            $commit['id_project'] = $project['id'];
            $this->syncCommit($commit, $repository);
        }
    }

    private function syncCommit(array $commit, ITicketsDataRepository $repository): void
    {
        $model = $this->createModelFromArray($commit);
        $existsModel = $repository->findByRemoteID($this->getIdent(), $model->getRemoteID());

        if ($existsModel) {
            return;
        }

        $repository->add($model);
    }

    private function createModelFromArray(array $data): ITicketsDataModel
    {
        $data['source_ident'] = $this->getIdent();
        $data['id_author'] = $this->credentials->getUser()->getID();

        return new GitlabIssueDataModel($data);
    }
}
