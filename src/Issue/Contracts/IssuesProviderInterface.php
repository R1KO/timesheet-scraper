<?php

namespace R1KO\TimesheetScraper\Contracts;

use DateTimeInterface;
use R1KO\TimesheetScraper\Repository\Contracts\DataRepositoryInterface;

interface IssuesProviderInterface extends ScraperProviderInterface
{
    public function run(
        IProviderCredentials $credentials,
        DataRepositoryInterface $repository,
        DateTimeInterface $dateFrom,
        DateTimeInterface $dateTo
    ): void;
}
