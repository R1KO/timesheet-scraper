<?php

namespace R1KO\TimesheetScraper;

use Exception;
use DateTimeInterface;
use R1KO\TimesheetScraper\Contracts\IssuesDataRepositoryInterface;
use R1KO\TimesheetScraper\Contracts\IssuesProviderInterface;
use R1KO\TimesheetScraper\Contracts\IssuesServicesManagerInterface;
use R1KO\TimesheetScraper\Contracts\ServiceInterface;

class IssuesScrapper
{
    public function run(
        RepositoryServicesManagerInterface $servicesManager,
        IssuesDataRepositoryInterface $repository,
        DateTimeInterface $dateFrom,
        DateTimeInterface $dateTo,
    ): void
    {
        foreach ($servicesManager->getAll() as $service) {
            $provider = $this->getProviderInstance($service);

            $provider->run(
                $service->getCredentials(),
                $repository,
                $dateFrom,
                $dateTo
            );
        }
    }

    private function getProviderInstance(ServiceInterface $service): IssuesProviderInterface
    {
        $className = $service->getClassname();
        if (!class_exists($className)) {
            throw new Exception(sprintf('Provider class "%s" not found!', $className));
        }

        $instance = new $className();
        if (!$instance instanceof IssuesProviderInterface) {
            throw new Exception(
                sprintf('Provider class "%s" is not implements "%s"!', $className, IssuesProviderInterface::class)
            );
        }

        return $instance;
    }
}
