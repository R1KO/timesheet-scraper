<?php

namespace R1KO\TimesheetScraper\Models;

class ServiceValuesObject
{
    public const TYPE_REPOSITORY = 'repository';
    public const TYPE_TICKETING  = 'ticketing';

    public const STATUS_ACTIVE  = 'active';

}
